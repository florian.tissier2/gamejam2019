﻿using System;
using UnityEngine;

public class ScoreCollider : MonoBehaviour
{
    [SerializeField] private float pointsForThisObstacle;
    [SerializeField] private float maxHeightJump;
    [SerializeField] private float threshold;

    public delegate void ObstacleJumped(float pointsWon);

    public static event ObstacleJumped OnObstaclePassed;

    public delegate void PerfectJump(string tag);

    public static event PerfectJump OnPerfectJump;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var bonusPerfectJump = 1;
        if (maxHeightJump != 0)
        {
            if (Math.Abs(other.transform.localPosition.y) >= maxHeightJump - threshold &&
                Math.Abs(other.transform.localPosition.y) <= maxHeightJump)
            {
                bonusPerfectJump *= 2;
                OnPerfectJump?.Invoke(other.gameObject.tag);
                Debug.Log("Perfect Jump ! ");
            }
        }

        OnObstaclePassed?.Invoke(pointsForThisObstacle * bonusPerfectJump);
    }
}