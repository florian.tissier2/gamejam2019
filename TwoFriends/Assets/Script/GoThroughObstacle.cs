﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoThroughObstacle : MonoBehaviour
{
    [SerializeField] private GameObject blockingForPlayer1;
    [SerializeField] private GameObject blockingForPlayer2;


    public void DeactivateARandomBlock()
    {
        if (Random.Range(0, 100) % 2 == 0)
        {
            blockingForPlayer1.SetActive(false);
        }
        else
        {
            blockingForPlayer2.SetActive(false);
        }
    }
}
